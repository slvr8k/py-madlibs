# Madlib with Python

This project is my first program using Python and it´s a simple madlib.

## Usage

You need download the basic_madlib.py and open cmd or terminal **(Remember that you need install python in your system)**.

```bash
python basic_madlib.py
```
After that execute the program you can look in the terminal the next lines:

```bash
La programación me parece muy _____. Y eso es algo que me emociona mucho porque es algo que amo _____, 
cosa que me  _____ ya que en un futuro pueda ser alguien famoso como _____

Escriba las palabras para completar la frase de arriba.
Palabra 1: 
```
And you should write all the words in the empty spaces. And finally the program return in the terminal the sentence with the words that you wrote. For example:
```bash
La programación me parece muy divertida. Y eso es algo que me emociona mucho porque es algo que amo hacer, 
cosa que me motiva ya que en un futuro pueda ser alguien famoso como Linus Torvalds
```